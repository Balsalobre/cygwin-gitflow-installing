# Instalación Cygwin

Navegamos a su página oficial http://www.cygwin.com/ y elegimos la instalación para nuestro sistema operativo.

Debemos seleccionar en el proceso de instalación el lugar donde queremos que se descargen todos los paquetes y dependencias de Cygwin. En este caso lo haremos en *"C:\Cygwing_packages"* 

![alt text](/resources/package_directory.png "Package Directory")

Una vez hecho esto podremos instalar los paquetes que necesitemos dentro de la propia aplicación.

![alt text](/resources/select_packages.png "Select Packages")

    1. git
    2. ssh --> openssh
    3. nano
    4. wget
    5. util-linux

Una vez tengamos seleccionados todos los paquetes que necesitamos pulsamos en siguiente y se procede a su instalación.
**Nota:** si fuese necesario instalar otro paquete, bastaría con reinstalar la consola indicando que paquetes queremos añadir.

## Creación de un alias

Navegamos a la carpeta donde tenemos instalado Cygwin en este caso *"C:\cygwin64\home\cbalsalobre"* y editamos el fichero [ .bash_profile ], añadiéndole un alias al final del fichero.

```
    alias nestjs-test="cd /cygdrive/c/Proyectos/Nestjs-test";
```

![alt text](/resources/alias.png "Adding alias")

De esta forma escribiendo nestjs-test acederíamos al directorio indicado.

# Instalación de git-flow

Nos vamos al [cheatsheet](https://danielkummer.github.io/git-flow-cheatsheet/index.es_ES.html) de git-flow y seguimos la instalación paso a paso.

![alt text](/resources/gitflow_init.png "Select Packages")
